package com.enable.conference.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Registration implements Serializable {
    private String name;
}
