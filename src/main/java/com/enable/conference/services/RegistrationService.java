package com.enable.conference.services;

import com.enable.conference.models.Registration;


import java.io.IOException;


public interface RegistrationService {
    void register(Registration registration) throws IOException;
    Registration read() throws IOException;
}
