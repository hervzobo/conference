package com.enable.conference.services;

import com.enable.conference.models.Registration;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;

@Service
public class RegistrationServiceImpl implements RegistrationService{
    @Override
    public void register(Registration registration) throws IOException {
        File file = ResourceUtils.getFile("classpath:data/registration.txt");
        try(FileOutputStream stream = new FileOutputStream(file);
            ObjectOutputStream outputStream = new ObjectOutputStream(stream)) {
            outputStream.writeObject(registration);
        }
    }

    @Override
    public Registration read() throws IOException {
        File file = ResourceUtils.getFile("classpath:data/registration.txt");
        try(FileInputStream stream = new FileInputStream(file);
            ObjectInputStream inputStream = new ObjectInputStream(stream)) {
            return (Registration) inputStream.readObject();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }
}
