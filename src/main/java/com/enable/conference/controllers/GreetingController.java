package com.enable.conference.controllers;

import com.enable.conference.models.Registration;
import com.enable.conference.services.RegistrationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


@Controller
public class GreetingController {
    private final RegistrationService registrationService;
    private static final Logger LOGGER = Logger.getLogger(GreetingController.class.getName());

    public GreetingController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping("greeting")
    public String greeting(Map<String, Object> model) {
       try {
           Registration registration = registrationService.read();
           model.put("message", "Hello, " + registration.getName());
       }catch (RuntimeException | IOException e){
           LOGGER.log(Level.SEVERE, e.getMessage(), e);
       }

        return "greeting";
    }
}
