package com.enable.conference.controllers;

import com.enable.conference.models.Registration;
import com.enable.conference.services.RegistrationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class RegistrationController {

    private final RegistrationService registrationService;
    private static final Logger LOGGER = Logger.getLogger(RegistrationController.class.getName());
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }


    @GetMapping("registration")
    public String getRegistration(@ModelAttribute("registration")Registration registration){
        return "registration";
    }

    @PostMapping("registration")
    public String postRegistration(@ModelAttribute("registration")Registration registration){
        System.out.println("registration = " + registration);
        try {
            if (Objects.isNull(registration)) {
                throw new RuntimeException("Registration object is null.");
            }
            registrationService.register(registration);
        }catch (RuntimeException | IOException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return "redirect:greeting";
    }
}
